//
//  MainViewModel.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 23/02/23.
//

import Foundation

protocol MainViewModelProtocol {
    var showProductNotFoundFeedback: (() -> Void)? { get set }
    var hideProductNotFoundFeedback: (() -> Void)? { get set }
    var hideKeyboard: (() -> Void)? { get set }
    var getProducts: (([ProductModel]) -> Void)? { get set }
    var reloadProductsTableViewData: (() -> Void)? { get set }
    func getProductsByName(_ name: String)
}

final class HomeViewModel: MainViewModelProtocol {
    var showProductNotFoundFeedback: (() -> Void)?
    var hideProductNotFoundFeedback: (() -> Void)?
    var hideKeyboard: (() -> Void)?
    var getProducts: (([ProductModel]) -> Void)?
    var reloadProductsTableViewData: (() -> Void)?
    
    private let dropperService = DroperService()
    private let productConverterService = ProductConverterService()
    
    func getProductsByName(_ name: String) {
        hideKeyboard?()
        dropperService.getSearchV3ByName(name) { [weak self] droperProducts, error in
            if let validDroperProducts = droperProducts?.produtos, let convertedProducts = self?.convertDroperProductModelToProductModel(validDroperProducts) {
                self?.hideProductNotFoundFeedback?()
                self?.getProducts?(convertedProducts)
                (convertedProducts.count == 0) ? self?.hideProductNotFoundFeedback?() : self?.showProductNotFoundFeedback?()
                self?.reloadProductsTableViewData?()
            } else {
                print(error?.localizedDescription ?? "error - \(MessagesEnum.errorDroperGeneric.rawValue)")
            }
        }
    }

    private func convertDroperProductModelToProductModel(_ droperProductModel: [DroperProductModel]) -> [ProductModel] {
        var products: [ProductModel] = []
        productConverterService.convertDroperProductModelToProductModel(droperProductModel) { convertedProducts, error in
            if let validConvertedProducts = convertedProducts {
                products = validConvertedProducts
            } else {
                print(error?.localizedDescription ?? "error - \(MessagesEnum.errorConvertDroperProductModelToProductModel.rawValue)")
            }
        }
        return products
    }
}
