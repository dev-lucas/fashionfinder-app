//
//  SearchViewModel.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 07/03/23.
//

import Foundation

protocol SearchViewModelProtocol {
    var showProductNotFoundFeedback: (() -> Void)? { get set }
    var hideProductNotFoundFeedback: (() -> Void)? { get set }
    var hideKeyboard: (() -> Void)? { get set }
    var getProductsIndexed: (([DropIndexedModel]) -> Void)? { get set }
    var reloadProductsTableViewData: (() -> Void)? { get set }
    func getProductsIndexedByName(_ name: String)
}

final class SearchViewModel: SearchViewModelProtocol {
    var showProductNotFoundFeedback: (() -> Void)?
    var hideProductNotFoundFeedback: (() -> Void)?
    var hideKeyboard: (() -> Void)?
    var getProductsIndexed: (([DropIndexedModel]) -> Void)?
    var reloadProductsTableViewData: (() -> Void)?
    
    private let dropperService = DroperService()
    private let productConverterService = ProductConverterService()
    
    func getProductsIndexedByName(_ name: String) {
        hideKeyboard?()
        dropperService.getSearchV3ByName(name) { [weak self] droperProducts, error in
            if let validDroperDrops = droperProducts?.drops, let convertedDrops = self?.convertDroperDropModelToDropModel(validDroperDrops) {
                let convertedProductsIndexed = self?.productConverterService.convertDropModelToDropIndexedModel(convertedDrops) ?? []
                self?.hideProductNotFoundFeedback?()
                self?.getProductsIndexed?(convertedProductsIndexed)
                (convertedProductsIndexed.count == 0) ? self?.hideProductNotFoundFeedback?() : self?.showProductNotFoundFeedback?()
                self?.reloadProductsTableViewData?()
            } else {
                print(error?.localizedDescription ?? "error - \(MessagesEnum.errorDroperGeneric.rawValue)")
            }
        }
    }

    private func convertDroperDropModelToDropModel(_ droperDropModel: [DroperDropModel]) -> [DropModel] {
        var drops: [DropModel] = []
        productConverterService.convertDroperDropModelToDropModel(droperDropModel) { convertedDrops, error in
            if let validConvertedDrops = convertedDrops {
                drops = validConvertedDrops
            } else {
                print(error?.localizedDescription ?? "error - \(MessagesEnum.generic.rawValue)")
            }
        }
        return drops
    }
}
