//
//  SceneDelegate.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 23/02/23.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.windowScene = windowScene
        
        let mainTabBarItem = UITabBarItem()
        mainTabBarItem.title = "Home"
        mainTabBarItem.image = UIImage(named: "market-place-icon")
        let mainViewController = HomeViewController(mainViewModel: HomeViewModel())
        mainViewController.tabBarItem = mainTabBarItem
        
        let searchTabBarItem = UITabBarItem()
        searchTabBarItem.title = "Pesquisa"
        searchTabBarItem.image = UIImage(named: "advanced-search-icon")
        let searchViewController = SearchViewController(searchViewModel: SearchViewModel())
        searchViewController.tabBarItem = searchTabBarItem
        
        let tabBarController = UITabBarController()
        tabBarController.viewControllers = [mainViewController, searchViewController]
        tabBarController.selectedViewController = mainViewController
        tabBarController.tabBar.barTintColor = .white
        tabBarController.tabBar.tintColor = .darkGray

        window?.rootViewController = tabBarController
        window?.makeKeyAndVisible()
    }

    func sceneDidDisconnect(_ scene: UIScene) { }

    func sceneDidBecomeActive(_ scene: UIScene) { }

    func sceneWillResignActive(_ scene: UIScene) { }

    func sceneWillEnterForeground(_ scene: UIScene) { }

    func sceneDidEnterBackground(_ scene: UIScene) { }
}
