//
//  ImageUtil.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 24/02/23.
//

import UIKit
import AlamofireImage

final class ImageUtil {
    static private let defaultImageUrl = "https://static.vecteezy.com/system/resources/previews/004/141/669/original/no-photo-or-blank-image-icon-loading-images-or-missing-image-mark-image-not-available-or-image-coming-soon-sign-simple-nature-silhouette-in-frame-isolated-illustration-vector.jpg"
    
    static func setImageViewByUrl(_ imageView: UIImageView?, _ url: String?) {
        if let url = URL(string: url ?? defaultImageUrl) {
            imageView?.af.setImage(withURL: url)
        }
    }
}
