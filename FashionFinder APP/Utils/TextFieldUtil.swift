//
//  TextFieldUtil.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 24/02/23.
//

import UIKit

final class TextFieldUtil {
    static private var targetView = UIView()
    
    static func hideKeybordOnOutsideTouch(_ view: UIView) {
        targetView = view
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.cancelsTouchesInView = false
        targetView.addGestureRecognizer(tap)
    }
    
    @objc static func hideKeyboard() {
        targetView.endEditing(true)
    }
}
