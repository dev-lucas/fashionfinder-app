//
//  ViewsUtil.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 28/02/23.
//

import UIKit

final class LabelUtil {
    static func simple(text: String, fontSettings: UIFont, labelSettings: LabelSettingsModel) -> UILabel {
        let labelView = UILabel()
        labelView.backgroundColor = labelSettings.backgroundColor
        labelView.textAlignment = labelSettings.textAlignment
        labelView.textColor = labelSettings.textColor
        labelView.text = text
        labelView.font = fontSettings
        labelView.layer.cornerRadius = labelSettings.cornerRadiusValue
        labelView.layer.masksToBounds = labelSettings.isMasksToBounds
        
        return labelView
    }
}
