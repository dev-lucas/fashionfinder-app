//
//  ProductSearchRecomendedTableViewCell.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 02/03/23.
//

import UIKit

class SearchRecommendedProductsTableViewCell: UITableViewCell {

    @IBOutlet var titlesLabels: [UILabel]?
    @IBOutlet var priceTitleLabels: [UILabel]?
    @IBOutlet var productPriceLabels: [UILabel]?
    @IBOutlet var productViews: [UIView]?
    @IBOutlet var productImages: [UIImageView]?
    private var allProducts: [ProductModel] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func settingCells(products: [DropIndexedModel]?, currentRow: Int, totalRows: Int) {
        let cellPerRow = TableViewCellEnum.searchRecommendedProductsCellPerRow.rawValue
        
        guard let productList = products,
              let titleLabelList = titlesLabels else {
            return
        }
        
        for i in 0..<titleLabelList.count {
            titleLabelList[i].text = ""
            productImages?[i].image = UIImage()
            priceTitleLabels?[i].text = ""
            productPriceLabels?[i].text = ""
            productViews?[i].layer.cornerRadius = 15
            productViews?[i].backgroundColor = .white.withAlphaComponent(0)
            productImages?[i].layer.cornerRadius = 15
        }

        var titleLabelListIndex = 0
        
        for i in 0..<productList.count {
            if ((currentRow == 0) && ((productList[i].index == 0) ||
                (productList[i].index == cellPerRow - 1)) ||
                (currentRow * cellPerRow == productList[i].index) ||
                (currentRow * cellPerRow == productList[i].index - 1))   {
                ImageUtil.setImageViewByUrl(productImages?[titleLabelListIndex], productList[i].image)
                productPriceLabels?[titleLabelListIndex].text = productList[i].retailPrice
                titleLabelList[titleLabelListIndex].text = productList[i].name
                priceTitleLabels?[titleLabelListIndex].text = "Menor preço"
                productViews?[titleLabelListIndex].backgroundColor = .white
                titleLabelListIndex += 1
            }
        }
    }
    
    private func setOutlets(index: Int, product: ProductModel) {
        titlesLabels?[index].text = product.name
    }
}
