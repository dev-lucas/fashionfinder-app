//
//  SearchViewController.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 01/03/23.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet weak var searchTextField: UITextField?
    @IBOutlet weak var productsTableView: UITableView?
    
    let numberOfCellPerRow = TableViewCellEnum.searchRecommendedProductsCellPerRow.rawValue /* temporary */
    private var productsIndexed: [DropIndexedModel] = []
    private var numberOfRows = 0
    private var searchViewModel: SearchViewModelProtocol
    
    init(searchViewModel: SearchViewModel) {
        self.searchViewModel = searchViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        searchViewModel = SearchViewModel()
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
    }
    
    @IBAction func searchTriggered(_ sender: UITextField) {
        searchViewModel.getProductsIndexedByName(sender.text ?? "")
    }
    
    private func initialSettings() {
        setupBind()
        tableViewSettings()
        textFieldSettings()
        searchViewModel.getProductsIndexedByName("adidas")
    }
    
    private func textFieldSettings() {
        TextFieldUtil.hideKeybordOnOutsideTouch(view)
    }
    
    private func tableViewSettings() {
        productsTableView?.register(UINib(nibName: "SearchRecommendedProductsTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchRecommendedProductsTableViewCell")
        productsTableView?.dataSource = self
        productsTableView?.delegate = self
//        productsTableView?.backgroundColor = .white
    }
    
    private func setupBind() {
        searchViewModel.showProductNotFoundFeedback = { [weak self] in
//            self?.noProductsFoundLabel?.isHidden = true
        }
        
        searchViewModel.hideProductNotFoundFeedback = { [weak self] in
//            self?.noProductsFoundLabel?.isHidden = false
        }
        
        searchViewModel.hideKeyboard = { [weak self] in
            self?.searchTextField?.resignFirstResponder()
        }
        
        searchViewModel.getProductsIndexed = { [weak self] products in
            self?.productsIndexed = products
        }
        
        searchViewModel.reloadProductsTableViewData = { [weak self] in
            self?.productsTableView?.reloadData()
        }
    }
}

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        numberOfRows = (productsIndexed.count.isMultiple(of: numberOfCellPerRow) ? (productsIndexed.count / numberOfCellPerRow) : ((productsIndexed.count / numberOfCellPerRow) + 1)) //Criar util para isso
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = productsTableView?.dequeueReusableCell(withIdentifier: "SearchRecommendedProductsTableViewCell") as? SearchRecommendedProductsTableViewCell else { fatalError("Generate cell error") }
        cell.settingCells(products: productsIndexed, currentRow: indexPath.row, totalRows: (numberOfRows - 1))
        cell.selectionStyle = .none
        return cell
    }
}

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(TableViewCellEnum.searchRecommendedProductsCellHeight.rawValue)
    }
}
