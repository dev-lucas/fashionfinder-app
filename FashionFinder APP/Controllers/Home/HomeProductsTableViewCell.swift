//
//  ProductTableViewCell.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 23/02/23.
//

import UIKit

class HomeProductsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageImageView: UIImageView?
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var detailsContainerView: UIView?
    @IBOutlet weak var priceLabel: UILabel?
    @IBOutlet weak var originView: UIView?
    @IBOutlet weak var originLabel: UILabel?
    @IBOutlet var paddingsView: [UIView]?
    @IBOutlet weak var sizesView: UIView?
    var avaliableSizes: [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {// "View did load"
        settingAvaliableSizes(avaliableSizes)
    }
    
    func settingCell(product: ProductModel) {
        ImageUtil.setImageViewByUrl(imageImageView, product.image)
        originLabel?.text = product.origin
        originLabel?.textColor = .black
        originView?.backgroundColor = .white
        originView?.layer.cornerRadius = 7
        titleLabel?.text = product.name
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.minimumScaleFactor = 0.5
        titleLabel?.numberOfLines = 0
        priceLabel?.text = product.price
        priceLabel?.textColor = .black
        titleLabel?.textColor = .darkGray
        imageImageView?.layer.cornerRadius = 12
        detailsContainerView?.backgroundColor = .white.withAlphaComponent(0)
        settingPaddings(paddingsView)
        avaliableSizes = product.avaliableSizes ?? []
    }
    
    private func settingAvaliableSizes(_ sizes: [String]) {
        let paddingWidth: CGFloat = 7
        let paddingHeight: CGFloat = 7
        let marginWidth: CGFloat = 5
        let marginHeight: CGFloat = 5
        let fontSettings = UIFont.systemFont(ofSize: 12, weight: .regular)
        var lastWidth: CGFloat = 0
        var lastHeight: CGFloat = 0
        
        sizesView?.layoutIfNeeded()
        sizesView?.layer.masksToBounds = true
        sizesView?.backgroundColor = .white.withAlphaComponent(0)
        sizesView?.subviews.forEach({ $0.removeFromSuperview() })
        
        for size in sizes {
            let sizeView = LabelUtil.simple(text: size, fontSettings: fontSettings, labelSettings: LabelSettingsModel())

            if lastWidth < sizesView?.frame.width ?? 0 {
                sizeView.sizeToFit()
                sizeView.frame = CGRect(x: lastWidth, y: lastHeight, width: (sizeView.frame.width + paddingWidth), height: (sizeView.frame.height + paddingHeight))
                lastWidth += sizeView.frame.width + marginWidth
            }
            
            if lastWidth > sizesView?.frame.width ?? 0 {
                sizeView.sizeToFit()
                lastWidth = 0
                lastHeight += sizeView.frame.height + paddingHeight + marginHeight
                sizeView.frame = CGRect(x: lastWidth, y: lastHeight, width: (sizeView.frame.width + paddingWidth), height: (sizeView.frame.height + paddingHeight))
                lastWidth += sizeView.frame.width + marginWidth
            }

            sizesView?.addSubview(sizeView)
        }
    }
    
    private func settingPaddings(_ paddings: [UIView]?) {
        if let validPaddings = paddings {
            for padding in validPaddings {
                padding.backgroundColor = .white.withAlphaComponent(0)
            }
        }
    }
}
