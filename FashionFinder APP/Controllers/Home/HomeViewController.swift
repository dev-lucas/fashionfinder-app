//
//  MainViewController.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 23/02/23.
//

import UIKit

final class HomeViewController: UIViewController {

    @IBOutlet weak var searchTextField: UITextField?
    @IBOutlet weak var productsTableView: UITableView?
    @IBOutlet weak var productsTableViewSpinnerActivityIndicator: UIActivityIndicatorView?
    @IBOutlet weak var noProductsFoundLabel: UILabel?
    private var products: [ProductModel] = []
    private let defaultCellHeight: CGFloat = 200
    private var homeViewModel: MainViewModelProtocol
    
    init(mainViewModel: MainViewModelProtocol) {
        self.homeViewModel = mainViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        homeViewModel = HomeViewModel()
        super.init(coder: coder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
    }
    
    @IBAction func searchTriggered(_ sender: UITextField) {
        homeViewModel.getProductsByName(sender.text ?? "")
    }
    
    private func initialSettings() {
        setupBind()
        tableViewSettings()
        textFieldSettings()
        homeViewModel.getProductsByName("adidas")
    }
    
    private func tableViewSettings() {
        productsTableView?.register(UINib(nibName: "HomeProductsTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeProductsTableViewCell")
        productsTableView?.dataSource = self
        productsTableView?.delegate = self
        productsTableViewSpinnerActivityIndicator?.hidesWhenStopped = true
        productsTableViewSpinnerActivityIndicator?.isHidden = true
    }
    
    private func textFieldSettings() {
        TextFieldUtil.hideKeybordOnOutsideTouch(view)
    }
    
    private func setupBind() {
        homeViewModel.showProductNotFoundFeedback = { [weak self] in
            self?.noProductsFoundLabel?.isHidden = true
        }
        
        homeViewModel.hideProductNotFoundFeedback = { [weak self] in
            self?.noProductsFoundLabel?.isHidden = false
        }
        
        homeViewModel.hideKeyboard = { [weak self] in
            self?.searchTextField?.resignFirstResponder()
        }
        
        homeViewModel.getProducts = { [weak self] products in
            self?.products = products
        }
        
        homeViewModel.reloadProductsTableViewData = { [weak self] in
            self?.productsTableView?.reloadData()
        }
    }
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = productsTableView?.dequeueReusableCell(withIdentifier: "HomeProductsTableViewCell") as? HomeProductsTableViewCell else { fatalError("Generate cell error") }
        cell.settingCell(product: products[indexPath.row])
        return cell
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return defaultCellHeight
    }
}
