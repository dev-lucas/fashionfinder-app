//
//  DropperService.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 23/02/23.
//

import Alamofire
import Foundation

final class DroperService {
    private let url: String = "https://service.cataloko.com/api/search/v3"
    private let headers: HTTPHeaders = ["Accept": "application/json"]
    
    func getSearchV3ByName(_ name: String, completion: @escaping (_ droperProducts: DroperSearchV3Model?, _ error: Error?) -> Void) {
        let parameters: Parameters = [
            "termo": name,
            "page": 0,
            "amount": 21
        ]
        
        AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseDecodable(of: DroperSearchV3Model.self) { response in
            switch response.result {
            case .success(let result):
                completion(result, nil)
                break
            case .failure(let error):
                completion(nil, error)
                break
            }
        }
    }
}
