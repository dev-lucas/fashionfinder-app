//
//  ProductConversorService.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 24/02/23.
//

import Foundation

final class ProductConverterService {
    func convertDroperProductModelToProductModel(_ droperProductModel: [DroperProductModel], completion: @escaping (_ convertedProducts: [ProductModel]?, _ error: Error?) -> Void) {
        var productList: [ProductModel] = []
        
        for product in droperProductModel {
            guard let name = product.nome,
                  let moeda = product.moeda,
                  let price = product.preco else {
                return completion(nil, ErrorEnum.runtimeError("Failed to convert DroperProductModel to ProductModel"))
            }
            productList.append(ProductModel(image: product.linkfoto ,name: name, price: "\(moeda) \(price)", origin: "droper", isAvaliable: true, avaliableSizes: product.TamanhoProdutoSelecao))
        }
        
        return completion(productList, nil)
    }
    
    func convertDropModelToDropIndexedModel(_ dropModel: [DropModel]) -> [DropIndexedModel] {
        var dropsIndexedModel: [DropIndexedModel] = []
        for i in 0..<dropModel.count {
            let dropIndexedModel = DropIndexedModel(index: i,
                                                       image: dropModel[i].image,
                                                       name: dropModel[i].title,
                                                       retailPrice: dropModel[i].retailPrice,
                                                       origin: nil,
                                                       isAvaliable: true,
                                                       avaliableSizes: nil)
            dropsIndexedModel.append(dropIndexedModel)
        }
        return dropsIndexedModel
    }
    
    func convertDroperDropModelToDropModel(_ droperDropModel: [DroperDropModel], completion: @escaping (_ convertedDrops: [DropModel]?, _ error: Error?) -> Void) {
        var dropList: [DropModel] = []
        
        for drop in droperDropModel {
            guard let title = drop.titulo,
                  let image = drop.icone,
                  let retailPrice = drop.precofRetail else {
                return completion(nil, ErrorEnum.runtimeError("Failed to convert DroperDropModel to DropModel"))
            }
            dropList.append(DropModel(title: title, image: image, retailPrice: "R$ \(retailPrice)"))
        }
        
        return completion(dropList, nil)
    }
}
