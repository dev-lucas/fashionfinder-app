//
//  TableViewCellEnum.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 07/03/23.
//

import Foundation

enum TableViewCellEnum: Int {
    case searchRecommendedProductsCellPerRow = 2
    case searchRecommendedProductsCellHeight = 220
}
