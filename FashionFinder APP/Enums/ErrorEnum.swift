//
//  ErrorEnum.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 27/02/23.
//

import Foundation

enum ErrorEnum: Error {
    case runtimeError(String)
}
