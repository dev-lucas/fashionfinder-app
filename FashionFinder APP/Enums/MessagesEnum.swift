//
//  MessagesEnum.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 06/03/23.
//

import Foundation

enum MessagesEnum: String {
    case errorDroperGeneric = "Failed to get DroperProductModel"
    case errorConvertDroperProductModelToProductModel = "Failed to convert DroperProductModel to ProductModel"
    case generic = "Failed to get response"
}
