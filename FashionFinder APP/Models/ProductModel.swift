//
//  ProductModel.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 24/02/23.
//

import Foundation

struct ProductModel {
    let image: String?
    let name: String
    let price: String
    let origin: String
    let isAvaliable: Bool
    let avaliableSizes: [String]?
}
