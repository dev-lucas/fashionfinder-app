//
//  DropperSearchV3Model.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 23/02/23.
//

import Foundation

struct DroperSearchV3Model: Decodable {
    let produtos: [DroperProductModel]?
    let drops: [DroperDropModel]?
}
 
