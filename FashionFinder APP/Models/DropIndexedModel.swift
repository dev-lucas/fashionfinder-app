//
//  ProductIndexedModel.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 05/03/23.
//

import Foundation

struct DropIndexedModel {
    let index: Int
    let image: String?
    let name: String
    let retailPrice: String
    let origin: String?
    let isAvaliable: Bool
    let avaliableSizes: [String]?
}
