//
//  DropperProductModel.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 23/02/23.
//

import Foundation

struct DroperProductModel: Decodable {
    let id: Int?
    let moeda: String?
    let preco: String?
    let nome: String?
    let linkfoto: String?
    let TamanhoProdutoSelecao: [String]?
}
