//
//  DropsModel.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 22/03/23.
//

import Foundation

struct DropModel {
    let title: String
    let image: String
    let retailPrice: String
}
