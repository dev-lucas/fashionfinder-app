//
//  DroperDropsModel.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 22/03/23.
//

import Foundation

struct DroperDropModel: Decodable {
    let titulo: String?
    let icone: String?
    let precofRetail: Double?
}
