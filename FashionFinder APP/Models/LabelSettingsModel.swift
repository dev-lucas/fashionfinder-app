//
//  LabelModel.swift
//  FashionFinder APP
//
//  Created by Lucas Gomes on 28/02/23.
//

import UIKit

final class LabelSettingsModel {
    var backgroundColor: UIColor = .gray
    var textAlignment: NSTextAlignment = .center
    var textColor: UIColor = .white
    var cornerRadiusValue: CGFloat = 5
    var isMasksToBounds: Bool = true
    
    init(backgroundColor: UIColor, textAlignment: NSTextAlignment, textColor: UIColor, cornerRadiusValue: CGFloat, isMasksToBounds: Bool) {
        self.backgroundColor = backgroundColor
        self.textAlignment = textAlignment
        self.textColor = textColor
        self.cornerRadiusValue = cornerRadiusValue
        self.isMasksToBounds = isMasksToBounds
    }
    
    init() { }
}
